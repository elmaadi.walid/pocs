import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MevService {
  private example_url = 'http://jsonplaceholder.typicode.com/posts';
  private mev_url = 'https://api.mev.quebec/1.0/';
  private headers = { 'X-API-KEY': 'VOTRE-CLE-API', 'X-MEV-ID': 'IDENTIFIANT-PASSERELLE-MEV' };

  constructor(private httpClient: HttpClient) { }

  getPosts() {
    return this.httpClient.get(this.example_url);
  }

  addPost(post_title: string) {
    return this.httpClient.post<any>(this.example_url, { title: post_title });
  }
}
