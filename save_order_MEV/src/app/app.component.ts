import { Component, OnInit } from '@angular/core';
import { MevService } from './services/mev.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'save_order_MEV';
  posts:any;

  constructor(private service:MevService) {}

  ngOnInit() {
    this.service.addPost('new post')
      .subscribe(data => {
        console.log(data.id);
        console.log(data.title);
    });
    this.service.getPosts()
      .subscribe(response => {
        this.posts = response;
      });
  }
}
